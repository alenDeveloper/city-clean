package com.project.nerol.cleancity.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.nerol.cleancity.R;
import com.project.nerol.cleancity.helper.ImageHelper;
import com.project.nerol.cleancity.model.CityEntry;

import java.util.ArrayList;
import java.util.List;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {

    private List<CityEntry> mainList = new ArrayList<>();
    private OnItemClick listener;

    public MainListAdapter(List<CityEntry> mainList, OnItemClick listener) {
        this.mainList = mainList;
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_list_row, parent, false);
        return new MainListAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        CityEntry entry = mainList.get(position);

        Bitmap image = ImageHelper.stringToBitmap(entry.imageAddress);
        if (image != null) {
            holder.mainImage.setImageBitmap(image);
        }
        holder.title.setText(entry.title);
        holder.address.setText(entry.address);
        holder.entry = entry;

    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mainImage;
        TextView title, address;
        CityEntry entry;

        public ViewHolder(View itemView, final OnItemClick listener) {
            super(itemView);

            mainImage = itemView.findViewById(R.id.list_image);
            title = itemView.findViewById(R.id.list_title);
            address = itemView.findViewById(R.id.list_address);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(entry);
                }
            });
        }
    }

    public interface OnItemClick {
        void onItemClick(CityEntry cityEntry);
    }
}

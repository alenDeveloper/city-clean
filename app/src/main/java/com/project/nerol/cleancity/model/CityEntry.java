package com.project.nerol.cleancity.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "CityEntry")
public class CityEntry extends Model {

    @Column(name = "Title")
    public String title;
    @Column(name = "Description")
    public String description;
    @Column(name = "Address")
    public String address;
    @Column(name = "Lat")
    public String lat;
    @Column(name = "Lng")
    public String lng;
    @Column(name = "ImageAddress")
    public String imageAddress;
    @Column(name = "Time")
    public String time;

    public static List<CityEntry> getAll() {
        return new Select().from(CityEntry.class).execute();
    }

    public static CityEntry getSpecific(Long title) {
        return (CityEntry) new Select()
                .from(CityEntry.class)
                .where("Id = ?", title)
                .execute()
                .get(0);
    }
}

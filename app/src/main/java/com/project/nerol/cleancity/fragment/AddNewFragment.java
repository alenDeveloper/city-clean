package com.project.nerol.cleancity.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.project.nerol.cleancity.R;
import com.project.nerol.cleancity.activity.MainActivity;
import com.project.nerol.cleancity.helper.ImageHelper;
import com.project.nerol.cleancity.model.CityEntry;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;

public class AddNewFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Boolean toShow;
    private Long itemId;

    private ImageView takeImage, mainImage;
    private Button saveBtn, cancelBtn;
    private EditText titleText, descriptionText;
    private TextView address, coordinate, time;

    private Bitmap currentImage = null;

    LocationManager locationManager;
    LocationListener locationListener;

    public AddNewFragment() {
        // Required empty public constructor
    }

    public static AddNewFragment newInstance(Boolean toShow, Long itemId) {
        AddNewFragment fragment = new AddNewFragment();
        Bundle args = new Bundle();
        if (toShow != null) {
            args.putBoolean(ARG_PARAM1, toShow);
        }
        if (itemId != null) {
            args.putLong(ARG_PARAM2, itemId);
        }
        fragment.setArguments(args);
        return fragment;
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            toShow = getArguments().getBoolean(ARG_PARAM1);
            itemId = getArguments().getLong(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new, container, false);
        ActiveAndroid.initialize(getActivity());

        takeImage = view.findViewById(R.id.take_image);
        mainImage = view.findViewById(R.id.main_image);
        saveBtn = view.findViewById(R.id.save_btn);
        cancelBtn = view.findViewById(R.id.cancel_btn);
        titleText = view.findViewById(R.id.input_title);
        descriptionText = view.findViewById(R.id.input_description);
        address = view.findViewById(R.id.entry_address);
        coordinate = view.findViewById(R.id.entry_coordinate);
        time = view.findViewById(R.id.entry_time);

        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (toShow && itemId != null) {
            CityEntry entry = CityEntry.getSpecific(itemId);
            takeImage.setClickable(false);
            mainImage.setClickable(false);
            Bitmap image = ImageHelper.stringToBitmap(entry.imageAddress);
            if (image != null) {
                mainImage.setImageBitmap(image);
            }

            titleText.setText(entry.title);
            titleText.setFocusable(false);
            titleText.setEnabled(false);

            descriptionText.setText(entry.description);
            descriptionText.setFocusable(false);
            descriptionText.setEnabled(false);

            address.setVisibility(View.VISIBLE);
            address.setText(entry.address);

            coordinate.setVisibility(View.VISIBLE);
            coordinate.setText(String.format("lat: %s lng: %s", entry.lat, entry.lng));

            time.setVisibility(View.VISIBLE);
            time.setText(entry.time);

            cancelBtn.setVisibility(View.GONE);
            saveBtn.setText("BACK");
        } else {
            address.setVisibility(View.GONE);
            coordinate.setVisibility(View.GONE);
            time.setVisibility(View.GONE);

            takeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dispatchTakePictureIntent();
                }
            });
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!toShow) {
                    saveEntry();
                }
                closeFragment();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeFragment();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            currentImage = imageBitmap;
            mainImage.setImageBitmap(imageBitmap);
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    private void saveEntry() {
        String title = titleText.getText().toString();
        String desc = descriptionText.getText().toString();
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String dateFormat = sdf.format(currentDate);

        Location currentLocation = getLastKnownLocation();
        CityEntry cityEntry = new CityEntry();

        if (currentLocation != null) {
            String lat = String.valueOf(currentLocation.getLatitude());
            String lng = String.valueOf(currentLocation.getLongitude());

            String address = getAddress(currentLocation);

            cityEntry.address = address;
            cityEntry.lat = lat;
            cityEntry.lng = lng;
        }

        String image = ImageHelper.bitmapToString(currentImage);

        cityEntry.description = desc;
        cityEntry.imageAddress = image;
        cityEntry.time = dateFormat;
        cityEntry.title = title;

        cityEntry.save();

    }

    public String getAddress(Location currentLocation) {
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);

            if(addresses != null && addresses.size() > 0) {
                String address = "";

                if (addresses.get(0).getSubThoroughfare() != null) {
                    address += addresses.get(0).getSubThoroughfare() + " ";
                }

                if (addresses.get(0).getThoroughfare() != null) {
                    address += addresses.get(0).getThoroughfare() + ", ";
                }

                if (addresses.get(0).getLocality() != null) {
                    address += addresses.get(0).getLocality() + ", ";
                }

                if (addresses.get(0).getPostalCode() != null) {
                    address += addresses.get(0).getPostalCode() + ", ";
                }

                if (addresses.get(0).getCountryName() != null) {
                    address += addresses.get(0).getCountryName();
                }

                return address;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    private void closeFragment() {
        MainActivity activity = (MainActivity) getActivity();

        if (activity != null && !activity.isFinishing() && isAdded()) {
            activity.closeFragment();
        }
    }
}

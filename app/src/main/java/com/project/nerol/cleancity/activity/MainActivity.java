package com.project.nerol.cleancity.activity;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListActivity;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.project.nerol.cleancity.R;
import com.project.nerol.cleancity.adapter.MainListAdapter;
import com.project.nerol.cleancity.fragment.AddNewFragment;
import com.project.nerol.cleancity.model.CityEntry;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton addNewEntryBtn;
    LinearLayout fragmentWrapper;
    RelativeLayout noEntryHolder;
    RecyclerView mainList;
    MainListAdapter.OnItemClick listener;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "To use this app, you have to add this permission", Toast.LENGTH_SHORT).show();
                return;
            }
            addNewEntryBtn.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getApplicationContext(), "To use this app, you have to add this permission", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActiveAndroid.initialize(this);

        addNewEntryBtn = findViewById(R.id.new_entry_button);
        fragmentWrapper = findViewById(R.id.main_fragment_wrapper);
        noEntryHolder = findViewById(R.id.no_entry_holder);
        mainList = findViewById(R.id.main_list_view);

        listener = new MainListAdapter.OnItemClick() {
            @Override
            public void onItemClick(CityEntry cityEntry) {
                openFragment(cityEntry.getId(), true);
            }
        };

        addNewEntryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               openFragment(null, false);
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            addNewEntryBtn.setVisibility(View.VISIBLE);
        }

        setMainList();
    }

    public void setMainList() {
        List<CityEntry> list = CityEntry.getAll();

        if (list == null || list.isEmpty()) {
            noEntryHolder.setVisibility(View.VISIBLE);
            return;
        } else {
            noEntryHolder.setVisibility(View.GONE);
        }

        MainListAdapter mainListAdapter = new MainListAdapter(list, listener);
        mainList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mainList.setAdapter(mainListAdapter);
        mainListAdapter.notifyDataSetChanged();
    }

    private void openFragment(Long itemId, Boolean isFromShow) {
        fragmentWrapper.setVisibility(View.VISIBLE);
        addNewEntryBtn.setVisibility(View.GONE);

        FragmentManager fm = getFragmentManager();
        AddNewFragment fragment = AddNewFragment.newInstance(isFromShow, itemId);

        fm.beginTransaction().add(R.id.main_fragment_wrapper, fragment, null).disallowAddToBackStack().commit();
    }

    public void closeFragment() {
        addNewEntryBtn.setVisibility(View.VISIBLE);
        getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.main_fragment_wrapper)).commit();
        fragmentWrapper.setVisibility(View.GONE);
        setMainList();
    }
}
